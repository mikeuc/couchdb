$.CouchApp(function(app) {
  $("form#add_product").submit(function(e) {
    e.preventDefault();
    var newProduct = {
      prod: $("#prod").val(),
      desc: $("#desc").val()
    };

    if (newProduct.prod.length > 0) {
      app.db.saveDoc(newProduct, {
        success: function(resp) {
          $("ul#my_products").append(
            '<li id="' +
              newProduct._id +
              '">' +
	      '<p class="prod">Product: ' +
              newProduct.prod +
              "</p>" +
              '<p class="desc">Description: ' +
              newProduct.desc +
              "</p>" +
              '<div class="link">' +
              '<a href="#" onclick="return false;"' +
              ' id="' +
              newProduct._rev +
              '">Delete</a>' +
              "</div>" +
              '<div class="clear"></clear>' +
              "</li>"
          );
          $("#" + newProduct._rev).click(function() {
            if (confirm("Are you sure you want to delete this product?")) {
              var delProduct = {
                _id: newProduct._id,
                _rev: newProduct._rev
              };
              app.db.removeDoc(delProduct, {});
              $("#" + newProduct._id)
                .show()
                .fadeOut(2000);
              var del_count = parseInt($("#product_count span").html(), 10);
              del_count--;
              $("#product_count span").html(del_count);
              return false;
            }
          });
          $("ul#my_products li:last")
            .hide()
            .fadeIn(1500);
 	  $("#prod").val("");
          $("#desc").val("");
          var product_count = parseInt($("#product_count span").html(), 10);
          product_count++;
          $("#product_count span").html(product_count);
        }
      });
    } else {
      alert("You must enter a description to create a new product!");
    }
  });

  app.view("get_products", {
    success: function(json) {
      json.rows.map(function(row) {
        $("ul#my_products").append(
          '<li id="' +
            row.value._id +
            '">' +
	    '<p class="prod">Product: ' +
            row.key +
            "</p>" +
            '<p class="desc">Description: ' +
            row.value.desc +
            "</p>" +
             '<div class="link">' +
              '<a href="#" onclick="return false;"' +
              ' id="' +
            row.value._rev +
              '">Delete</a>' +
              "</div>" +
              '<div class="clear"></clear>'  +
            "</li>"
        );
        $("#" + row.value._rev).click(function() {
          if (confirm("Are you sure you want to delete this product?")) {
            var delProduct = {
              _id: row.value._id,
              _rev: row.value._rev
            };
            app.db.removeDoc(delProduct, {});
            $("#" + row.value._id)
              .show()
              .fadeOut(2000);
            var del_count = parseInt($("#product_count span").html(), 10);
            del_count--;
            $("#product_count span").html(del_count);
            return false;
          }
        });
      });
      $("#product_count span").html(json.rows.length);
    }
  });
});
